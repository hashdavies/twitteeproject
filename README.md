<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About The project

TWITEE is a mini and substandard runoff of Twitter. Users register and log in and can put up
anything that crosses their minds. The whole world can view their twits and comment on their
twits /and like them. We want you to build this project using PHP/Laravel as your backend tool.

 
## DETAILED DESIGN
Please follow the detailed technical design below as closely as possible.
● Each user should have an account detail containing their name, email, password, and
date created
● Your app should send an onboarding email to the user
● Every authenticated user can comment or like every post that is posted.
● Only the user that owns a post can delete that particular post.
● Users can log out and get redirected to the login route
● Unauthenticated users cannot interface with the app.
 
## Built With

This project use The follwing : 
Laravel 8 
Mysql Database
laravel/sanctum for Authentication


## How to Run the Project

1. copy the content of env.example and create .env file or run this command composer post-root-package-install to create the env file
2. Edit the env file with correct database credential and make sure the database as been created 
3. Run the command composer install
4. run this command " composer migrate:seed " to run the migration and seed the database with some record
5. then run php artisain serve to start your server. 

### Api Documenntation

- **[Documentation](https://documenter.getpostman.com/view/3557887/UyxjHSat)**

 

## Please Note ::::
I Also Added a relationship between twit and comment. When user load twit . it will load  comment that the twit has .