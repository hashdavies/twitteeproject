<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::factory()->count(4)->hasTwits(10)->create();
        $user = User::factory()->count(3)->hasTwits(5)->create();
        $user = User::factory()->count(2)->create();

    }
}
