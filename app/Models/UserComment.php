<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserComment extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'comment',
        'user_id',
        'twits_id',
   
     ];

    public function Userpost(){
        return $this->belongsTo(Twits::class);
    }
}

