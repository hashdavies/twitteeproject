<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTwitsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
 
       $user=$this->user();
     
       if($user==null){
           return false;
         }
         return true;
     }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'post' =>['required', 'string'],
            'user_id' =>['required', 'integer'],
        ];
    }
    protected function prepareForValidation()
    {

        $this->merge([
            'user_id' =>auth('sanctum')->user()->id,
            // 'user_id' => 2
        ]);
    }
}
