<?php

namespace App\Http\Resources\V1;

// use App\Http\Resources\V1\UserResource;
// use App\Models\UserComment;
use Illuminate\Http\Resources\Json\JsonResource;

class TwitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'post' => $this->post,
       
        ];
    }
}
