<?php

 namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\WelcomeEmailNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Foundation\Auth\User;


class AuthenticationController extends Controller
{
    //
    public function createAccount(Request $request)
     {
        $attr = $request->validate([
             'name' => '',
             'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6',
         ]);
        $UserEmail=$attr['email'];
$emailArray= explode("@", $UserEmail);
        $user = User::create([
            'name' => $attr['name'],
            'password' => bcrypt($attr['password']),
            'email' => $UserEmail,
            'username' =>$emailArray[0] ,
        ]);

             $response=   [
            'token' => $user->createToken('tokens')->plainTextToken,
            'userDetails' => $user,
          ];
          $user->notify(new WelcomeEmailNotification());

        //   return $user;
          return response()->json($response,200);


    }
    public function signin(Request $request)
    {
        $attr = $request->validate([
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        ]);

        if (!Auth::attempt($attr)) {
            $response['message']='Invalid Credentials';
            return response()->json($response,401);
         }
         $response=   [
            'token' => $request->user()->createToken('tokens')->plainTextToken,
            'userDetails' =>$request->user(),
          ];
     


        return $response;
       
    }
    public function signout(Request $request)
    {
        $request->user()->tokens()->delete();

        return [
            'message' => 'Tokens Revoked'
        ];
    }

}
