<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTwitsRequest;
use App\Http\Requests\UpdateTwitsRequest;

use App\Http\Resources\V1\TwitResource;
use App\Models\Twits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class TwitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $UserTwits= Twits::with('Usercomment')->get();
         return $UserTwits;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTwitsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTwitsRequest $request)
    {
         // print_r($request->all());
        return new TwitResource(Twits::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Twits  $twits
     * @return \Illuminate\Http\Response
     */
    public function show(Twits $twits)
    {
        //
        // return $twits;
        return new  TwitResource($twits);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Twits  $twits
     * @return \Illuminate\Http\Response
     */
    public function edit(Twits $twits)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTwitsRequest  $request
     * @param  \App\Models\Twits  $twits
     * @return \Illuminate\Http\Response
     *
     */
    public function update(UpdateTwitsRequest $request, Twits $twits)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Twits  $twits
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,int $id)
    {
        $twits =Twits::where('id',$id)->first();

        if(!$twits){
            return \response()->json(['error'=> 'twits with the given id not found']);
        }
        $can_delete = $request->user()->id == $twits->user_id ;

        if ($can_delete) {

            Twits::where('id',$id)->delete();
            // return new  TwitResource($twits);
            return response()->json(null,204);
        } else {
            return response()->json(['error' => 'You can only delete your own Twit.'], 403);
        }


        //
    }
}
