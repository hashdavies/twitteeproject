<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\StoreUserCommentRequest;
use App\Http\Requests\UpdateUserCommentRequest;
use App\Models\UserComment;
 use App\Http\Controllers\Controller;
use App\Http\Resources\V1\UserCommentResource;

class UserCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserCommentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserCommentRequest $request)
    {
        //
        // print_r($request->all());
        return new UserCommentResource(UserComment::create($request->all()));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserComment  $userComment
     * @return \Illuminate\Http\Response
     */
    public function show(UserComment $userComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserComment  $userComment
     * @return \Illuminate\Http\Response
     */
    public function edit(UserComment $userComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserCommentRequest  $request
     * @param  \App\Models\UserComment  $userComment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserCommentRequest $request, UserComment $userComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserComment  $userComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserComment $userComment)
    {
        //
    }
}
