<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/me', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'v1','namespace' => 'App\Http\Controllers\Api\V1','middleware' =>'auth:sanctum'], function () {

    Route::apiResource('twits', TwitsController::class);
    Route::apiResource('comments', UserCommentController::class);
        Route::post('/logout',  'AuthenticationController@signout');
});

Route::group(['prefix' => 'v1','namespace' => 'App\Http\Controllers\Api\V1'], function () {
    Route::post('/register', 'AuthenticationController@createAccount');
    Route::post('/signin', 'AuthenticationController@signin');

});

